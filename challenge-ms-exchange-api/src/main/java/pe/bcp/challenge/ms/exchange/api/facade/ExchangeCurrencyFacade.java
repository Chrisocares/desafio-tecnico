package pe.bcp.challenge.ms.exchange.api.facade;

import io.reactivex.Single;
import org.springframework.stereotype.Component;
import pe.bcp.challenge.ms.exchange.api.dto.RequestExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.RequestNewRateExchange;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseNewRateExchange;
import pe.bcp.challenge.ms.exchange.api.service.ExchangeService;

@Component
public class ExchangeCurrencyFacade {

  private final ExchangeService exchangeService;

  public ExchangeCurrencyFacade(ExchangeService exchangeService) {
    this.exchangeService = exchangeService;
  }

  public Single<ResponseExchangeCurrency> calculateExchangeRate(RequestExchangeCurrency requestExchangeCurrency) {
    return exchangeService.calculateRateExchange(requestExchangeCurrency);
  }

  public Single<ResponseNewRateExchange> createNewRateExchangeRate(RequestNewRateExchange requestNewRateExchange) {
    return exchangeService.createRateExchange(requestNewRateExchange);
  }

}
