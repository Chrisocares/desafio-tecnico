package pe.bcp.challenge.ms.exchange.api.mapper;

import io.reactivex.Single;
import org.springframework.stereotype.Service;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseNewRateExchange;
import pe.bcp.challenge.ms.exchange.domain.dto.NewRateExchangeResult;
import pe.bcp.challenge.ms.exchange.domain.dto.RateExchangeResult;

@Service
public class ExchangeCurrencyMapper {

  public Single<ResponseExchangeCurrency> mapperToResponse(RateExchangeResult rateExchangeResult) {
    return Single.create(singleSubscriber -> {
      ResponseExchangeCurrency responseExchangeCurrency = new ResponseExchangeCurrency();
      responseExchangeCurrency.setRateExchange(rateExchangeResult.getRateExchange());
      responseExchangeCurrency.setAmountOrigin(rateExchangeResult.getAmountOrigin());
      responseExchangeCurrency.setAmountExchange(rateExchangeResult.getAmountExchange());
      responseExchangeCurrency.setCurrencyDestination(rateExchangeResult.getCurrencyDestination());
      responseExchangeCurrency.setCurrencyOrigin(rateExchangeResult.getCurrencyOrigin());
      singleSubscriber.onSuccess(responseExchangeCurrency);
    });
  }

  public Single<ResponseNewRateExchange> mapperToResponseNewRate(NewRateExchangeResult newRateExchangeResult) {
    return Single.create(singleSubscriber -> {
      ResponseNewRateExchange response = new ResponseNewRateExchange();
      response.setAmountExchange(newRateExchangeResult.getAmountExchange());
      response.setChannel(newRateExchangeResult.getChannel());
      response.setCurrencyDestination(newRateExchangeResult.getCurrencyDestination());
      response.setCurrencyOrigin(newRateExchangeResult.getCurrencyOrigin());
      response.setDescription(newRateExchangeResult.getDescription());
      singleSubscriber.onSuccess(response);
    });
  }
}
