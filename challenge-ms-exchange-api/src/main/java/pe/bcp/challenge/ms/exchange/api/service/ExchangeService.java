package pe.bcp.challenge.ms.exchange.api.service;

import io.reactivex.Single;
import pe.bcp.challenge.ms.exchange.api.dto.RequestExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.RequestNewRateExchange;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseNewRateExchange;

public interface ExchangeService {

  Single<ResponseExchangeCurrency> calculateRateExchange(RequestExchangeCurrency requestExchangeCurrency);
  Single<ResponseNewRateExchange> createRateExchange(RequestNewRateExchange requestNewRateExchange);

}
