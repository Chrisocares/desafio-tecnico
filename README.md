
# **API TIPO DE CAMBIO**

## ARQUITECTURA GENERAL
![arquitectura](https://firebasestorage.googleapis.com/v0/b/urp2019tesis.appspot.com/o/arquitectura-asis.JPG?alt=media&token=aa2d0789-edbc-438b-9d9d-9ea1dcd65a0c)
 
1. Aplicación web : Componente desarrollado en angular que tendra comunicación directa al backend mediante el protocolo HTTP.
2. Aplicación backend : Componente desarrollado en el framework Spring Boot que expondra la capa de servicios rest al frontend para la interacción de datos con el usuario.
3. Base de datos : Componente que persitira la información de las funcionalidades de tipo de cambio.
Todas las aplicaciones estaran desplegados en contenedores.
 
## ESTRUCTURA LOGICA BACKEND
 
 
 1. **CONTROLLER** : Todo componente que expone apis REST u otro tipo de protocolo de comunicación.
 2. **DTO** : Es una capa de transporte usada para transportar información orientada a objetos.
 3. **SERVICE** : Interface que maneja logica a nivel de capa de API.
 5. **DAO** : Capa de acceso a datos donde se construyen logica funcional relacionado al manejo de entidades.
 6. **DOMAIN** : Representa una abstracción entre una tabla de base de datos y el dominio de la aplicación.
 7. **FACADE** : Capa que abstrae las responsabilidades de la capa service.
 
![estructura logica](https://firebasestorage.googleapis.com/v0/b/urp2019tesis.appspot.com/o/arquetipo-logico.JPG?alt=media&token=4dbb4c87-f86c-467f-b442-aa62e0078137)
  
  
## ESTRUCTURA FÍSICA (PAQUETERÍA)

![estructura fisica](https://firebasestorage.googleapis.com/v0/b/urp2019tesis.appspot.com/o/arquetipo-fisico.JPG?alt=media&token=7f1cd0a2-80bb-4cf0-93f1-a6223288546e)

## ARQUITECTURA IDEAL

![Arquitectura_ideal](https://firebasestorage.googleapis.com/v0/b/urp2019tesis.appspot.com/o/arquitectura-tobe.JPG?alt=media&token=f58d88f3-b9f6-4327-8097-a447c199261a)

## Funcionalidad

El proyecto expone las siguientes funcionalidades : 
1. Calcular el tipo de cambio en base a una moneda origen y moneda destino.
2. Ingresar un nuevo tipo de cambio en base a una moneda.

## Endpoints

Calculo de tipo de cambio : 
```
http://localhost:8080/exchangeCurrency/calculate
```

Ingresar nuevo tipo de cambio : 
```
http://localhost:8080/exchangeCurrency/new
```
